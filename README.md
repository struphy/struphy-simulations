# Struphy simulations <img src="dog-cartoon-struphy.jpg" width="150" align="right"> 

Find parameter files for all kinds of plasma simulations with the open source code [Struphy](https://gitlab.mpcdf.mpg.de/struphy/struphy).

After downloading a file `your_choice.yml` you can run the simulation as follows:

1. Go to the folder where you downloaded the file.
2. Type `struphy --set-i .`; then check if the input path is correct via `struphy -p`. You can now leave the folder.
3. Assuming the parameter file is for the model `MODEL` (check the [list of Struphy models](https://struphy.pages.mpcdf.de/struphy/sections/models.html)), you can run the simulation via
```
struphy run MODEL -i your_choice.yml
```
