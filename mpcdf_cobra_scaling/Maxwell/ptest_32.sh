#!/bin/bash -l
# Standard output and error:
#SBATCH -o /cobra/u/bkna/ptest_maxwell/slurm-p_32-%j.out
#SBATCH -e /cobra/u/bkna/ptest_maxwell/slurm-p_32-%j.err
# Initial working directory:
#SBATCH -D /cobra/u/bkna/
# Job Name:
#SBATCH -J p_32
#
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=32
# For OpenMP:
#SBATCH --cpus-per-task=1
#
#SBATCH --mail-type=none
#SBATCH --mail-user=byungkyu.na@ipp.mpg.de
#
# Memory limit for the job
#SBATCH --mem=32000
# Wall clock limit:
#SBATCH --time=24:00:00

module purge
module load gcc/9 openmpi anaconda/3/2021.11 mpi4py git

# load Python virtual environment
source struphy202307/env/bin/activate

# Set the number of OMP threads *per process* to avoid overloading of the node!
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

# For pinning threads correctly
# export OMP_PLACES=cores

# suppress warnings
OMPI_MCA_mpi_warn_on_fork=0
export OMPI_MCA_mpi_warn_on_fork