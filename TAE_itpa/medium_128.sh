#!/bin/bash -l
# Standard output and error:
#SBATCH -o /cobra/u/username/struphy-simulations/TAE_itpa/medium_128-%j.out
#SBATCH -e /cobra/u/username/struphy-simulations/TAE_itpa/medium_128-%j.err
# Initial working directory:
#SBATCH -D /cobra/u/username/
# Job Name:
#SBATCH -J medium_128
#
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=32
# For OpenMP:
#SBATCH --cpus-per-task=1
#
#SBATCH --mail-type=none
#SBATCH --mail-user=username@ipp.mpg.de
#
#SBATCH --mem=85000
#
# Wall clock limit:
#SBATCH --time=24:00:00

module purge
module load gcc/12 openmpi/4 anaconda/3/2023.03 mpi4py

# load Python virtual environment
source struphy/env/bin/activate

# suppress warnings
OMPI_MCA_mpi_warn_on_fork=0
export OMPI_MCA_mpi_warn_on_fork


